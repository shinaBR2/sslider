## Getting started

1. Build library
  * Run `npm install` to get the project's dependencies
  * Run `npm run build` to produce minified version.
2. Development mode
  * Having all the dependencies installed run `npm run dev`. This command will generate an non-minified version of your library and will run a watcher so you get the compilation on file change.

## Logs

31/7/2017
- Fix bugs with dots and active/disable classes
- Change function add/remove item classes from change
- Change gotoItem using goFirst/goLast

30/7/2017
- Fix bugs with dots in gotoItem
- Remove page definition


29/7/2017
Supported:
- onInit, onTransform, onTransformed callbacks
- Not enough item for a slide

Unsupported:
- Keyboard
- Touch
- onDestroyed, onResized, onRefreshed callbacks

Bugs:
- Has some bugs about dots
- Loop with not enough item