(function webpackUniversalModuleDefinition(root, factory) {
	if(typeof exports === 'object' && typeof module === 'object')
		module.exports = factory();
	else if(typeof define === 'function' && define.amd)
		define("SSlider", [], factory);
	else if(typeof exports === 'object')
		exports["SSlider"] = factory();
	else
		root["SSlider"] = factory();
})(this, function() {
return /******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _event = __webpack_require__(1);

var _event2 = _interopRequireDefault(_event);

var _browser = __webpack_require__(2);

var _browser2 = _interopRequireDefault(_browser);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var SSlider = function () {
  function SSlider(options) {
    _classCallCheck(this, SSlider);

    this.options = Object.assign({}, {
      width: '100%',
      height: null,
      nav: true,
      loop: true,
      dots: false,
      keyboard: false,
      touch: false,
      itemsPerPage: 1,
      startPage: 1,
      slideBy: 1,
      margin: 0,
      duration: '0.5s'
    }, options);

    this.initSlide();
  }

  _createClass(SSlider, [{
    key: 'initSlide',
    value: function initSlide() {
      var dealRatio = 1.6;
      var _class = {
        mainContainer: 'sslider',
        slideContainer: 'sslider__container',
        baseSlider: 'sslider__base',
        baseSliderTransform: 'sslider__base--transform',
        item: 'sslider__item',
        itemSingle: 'sslider__item--single',
        itemActive: 'sslider__item--active',
        navBar: 'sslider__navbar',
        next: 'sslider__next',
        prev: 'sslider__prev',
        dots: 'sslider__dots',
        dot: 'sslider__dot',
        dotActive: 'sslider__dot--active'
      };
      var elId = this.options.elmentId;
      var baseSlider = document.getElementById(elId);
      var childs = baseSlider.children;
      var totalItem = baseSlider.children.length;

      var containerWidth = this.options.width;
      var childClass = _class.item;
      var startPage = this.options.startPage;
      var itemsPerPage = this.options.itemsPerPage;
      var activeItemClass = _class.itemActive;
      var margin = this.options.margin;
      var firstActive = itemsPerPage * (startPage - 1);
      var lastActive = itemsPerPage * startPage;
      var itemWidth = (parseInt(containerWidth, 10) - (itemsPerPage - 1) * margin) / itemsPerPage;

      this._class = _class;
      this.ratio = dealRatio;
      this.mainContainer = document.createElement('div');
      this.slideContainer = document.createElement('div');
      this.baseSlider = baseSlider;
      this.currentPage = this.options.startPage;
      this.totalPage = Math.ceil(totalItem / this.options.itemsPerPage);
      this.maxIndex = totalItem;
      this.isTransform = _browser2.default.isSupport('transition');
      this.currentOffset = -(firstActive * (itemWidth + margin) + margin);
      this.firstOffset = -margin;
      this.lastOffset = -margin - (itemWidth + margin) * (totalItem - 1);
      this.currentIndex = firstActive;
      this.baseDistance = this.options.slideBy * (itemWidth + margin);
      this.loopDistance = (itemWidth + margin) * totalItem;
      this.isCarousel = itemsPerPage === 1 && this.options.slideBy === 1;
      this.isRunning = false;

      /**
       * Callback function
       */
      if (typeof this.options.onInit !== 'undefined') {
        this.options.onInit();
      }

      /**
       * Wrap to container
       */
      _event2.default.wrap(this.baseSlider, this.slideContainer);
      _event2.default.wrap(this.slideContainer, this.mainContainer);
      _event2.default.addClass(this.baseSlider, this._class.baseSlider);

      /**
       * 1. Calculate container width, height
       *    - If height was given by px, mainContainer get that value for it's height
       *    - If it was not, mainContainer's height will be calculte
       *    base on it's width with defined ratio
       * 2. Add CSS class to container
       */
      this.mainContainer.className = this._class.mainContainer;
      this.slideContainer.className = this._class.slideContainer;
      this.mainContainer.style.width = containerWidth;

      if (this.options.height !== null) {
        this.mainContainer.style.height = this.options.height;
      } else {
        var _w = this.mainContainer.style.width;

        if (_w.indexOf('%') > -1) {
          this.mainContainer.style.height = parseInt(_w, 10) * window.innerWidth / (100 * this.ratio) + 'px';
        } else {
          this.mainContainer.style.height = parseInt(_w, 10) / this.ratio + 'px';
        }
      }

      /**
       * Add CSS classes for items
       */
      /**
       * If loop, re-calculate baseSlider's width
       */
      if (this.options.loop) {
        var fragmentBefore = document.createDocumentFragment();
        var fragmentAfter = document.createDocumentFragment();
        var cloneFirst = void 0;
        var cloneLast = void 0;

        if (this.isCarousel) {
          cloneFirst = childs[0].cloneNode(true);
          cloneLast = childs[totalItem - 1].cloneNode(true);
          cloneFirst.removeAttribute('id');
          cloneLast.removeAttribute('id');
          fragmentAfter.insertBefore(cloneFirst, fragmentAfter.firstChild);
          fragmentBefore.appendChild(cloneLast);
          this.currentOffset = this.currentOffset - (itemWidth + margin);
          this.firstOffset = this.firstOffset - (itemWidth + margin);
          this.lastOffset = this.firstOffset - (itemWidth + margin) * (totalItem - 1);
          this.currentIndex = this.currentIndex + 1;
        } else {
          // TODO with multiple items per page and slide by other
        }

        this.baseSlider.insertBefore(fragmentBefore, this.baseSlider.firstChild);
        this.baseSlider.appendChild(fragmentAfter);
      }
      for (var i = 0, len = childs.length; i < len; i++) {
        _event2.default.addClass(childs[i], childClass);
        var imgSrc = childs[i].querySelector('img').src;

        childs[i].style.width = itemWidth + 'px';
        childs[i].style.marginLeft = margin + 'px';
        childs[i].style.backgroundImage = 'url(' + imgSrc + ')';
        childs[i].style.backgroundRepeat = 'no-repeat';
        childs[i].style.backgroundSize = 'cover';

        if (i >= firstActive && i < lastActive) {
          _event2.default.addClass(childs[i], activeItemClass);
        }
      }
      this.baseSlider.style.width = this.baseSlider.children.length * (itemWidth + margin) + 'px';

      /**
       * Add CSS classes for baseSlider
       */
      if (this.isTransform) {
        this.baseSlider.style.WebkitTransform = 'translateX(' + this.currentOffset + 'px)';
        this.baseSlider.style.transform = 'translateX(' + this.currentOffset + 'px)';
        this.baseSlider.style.WebkitTransitionDuration = this.options.duration;
        this.baseSlider.style.transitionDuration = this.options.duration;
        _event2.default.addClass(this.baseSlider, this._class.baseSliderTransform);
      } else {
        this.baseSlider.style.left = this.currentOffset + 'px';
      }

      /**
       * Next/prev button
       */
      if (this.options.nav) {
        var navBarHtml = '<button class=\'' + this._class.prev + '\'>&#10092;</button>\n          <button class=\'' + this._class.next + '\'>&#x276D;</button>';
        var navBar = document.createElement('div');

        navBar.innerHTML = navBarHtml;
        _event2.default.addClass(navBar, this._class.navBar);
        this.mainContainer.appendChild(navBar);

        var prevButton = navBar.querySelector('.' + this._class.prev);
        var nextButton = navBar.querySelector('.' + this._class.next);

        _event2.default.addEvent('click', prevButton, this.prev.bind(this));
        _event2.default.addEvent('click', nextButton, this.next.bind(this));
      }

      /**
       * Dots
       */
      if (this.options.dots) {
        var dotsHtml = '';
        var dots = document.createElement('div');

        for (var _i = 0; _i < this.totalPage; _i++) {
          if (_i === this.currentPage) {
            dotsHtml += '<button class=\'' + this._class.dot + ' ' + this._class.dotActive + '\'></button>';
          } else {
            dotsHtml += '<button class=\'' + this._class.dot + '\'></button>';
          }
        }

        dots.innerHTML = dotsHtml;
        _event2.default.addClass(dots, this._class.dots);
        this.mainContainer.appendChild(dots);
      }

      /**
       * Keyboard
       */
      if (this.options.keyboard) {}
      // TODO


      /**
       * Touch
       */
      if (this.options.touch) {}
      // TODO


      /**
       * Callback function
       */
      if (typeof this.options.onInited !== 'undefined') {
        this.options.onInited();
      }
    }
  }, {
    key: 'getCurrentPage',
    value: function getCurrentPage() {
      return this.currentPage;
    }
  }, {
    key: 'getTotalPage',
    value: function getTotalPage() {
      return this.totalPage;
    }
  }, {
    key: 'go',
    value: function go(offset, direction, duration) {
      var _this = this;

      return new Promise(function (resolve, reject) {
        if (_this.isRunning) {
          reject();
        } else {
          _this.isRunning = true;
          _this.currentOffset = offset;

          if (_this.isTransform) {
            _this.baseSlider.style.WebkitTransitionDuration = duration;
            _this.baseSlider.style.transitionDuration = duration;
            _this.baseSlider.style.WebkitTransform = 'translateX(' + _this.currentOffset + 'px)';
            _this.baseSlider.style.transform = 'translateX(' + _this.currentOffset + 'px)';
          } else {
            _this.baseSlider.style.left = _this.currentOffset + 'px';
          }

          setTimeout(function () {
            _this.isRunning = false;
            resolve();
          }, parseFloat(duration, 10) * 1000);
        }
      });
    }
  }, {
    key: 'goFirst',
    value: function goFirst() {
      // TODO
      // this.go(Math.abs(this.currentOffset), 'backward');
    }
  }, {
    key: 'goLast',
    value: function goLast() {
      // TODO
      // this.go(Math.abs(this.currentOffset), 'forward');
    }
  }, {
    key: 'next',
    value: function next() {
      var _this2 = this;

      if (!this.isRunning) {
        var maxIndex = this.maxIndex;
        var duration = this.options.duration;
        var slideBy = this.options.slideBy;
        var childs = this.baseSlider.children;
        var itemActiveClass = this._class.itemActive;
        var currentIndex = this.currentIndex;
        var firstActive = currentIndex + slideBy;

        if (this.isCarousel) {
          if (this.options.loop) {
            if (currentIndex === maxIndex) {
              var offset = this.currentOffset - this.baseDistance;
              var go = this.go(offset, 'forward', duration);

              go.then(function () {
                _this2.go(_this2.firstOffset, 'forward', '0s');
              }).then(function () {
                _event2.default.removeClass(childs[currentIndex], itemActiveClass);
                _event2.default.addClass(childs[firstActive], itemActiveClass);
                _this2.currentIndex = 1;
              }).catch(function () {
                return;
              });
            } else {
              var _offset = this.currentOffset - this.baseDistance;
              var _go = this.go(_offset, 'forward', duration);

              _go.then(function () {
                _event2.default.removeClass(childs[currentIndex], itemActiveClass);
                _event2.default.addClass(childs[firstActive], itemActiveClass);
                _this2.currentIndex = firstActive;
              }).catch(function () {
                return;
              });
            }
          } else {
            if (currentIndex === maxIndex - 1) {
              return;
            }

            var _offset2 = this.currentOffset - this.baseDistance;

            this.go(_offset2, 'forward', duration).then(function () {
              _event2.default.addClass(childs[currentIndex + 1], itemActiveClass);
              _event2.default.removeClass(childs[currentIndex], itemActiveClass);
              _this2.currentIndex = _this2.currentIndex + 1;
            }).catch(function () {
              return;
            });
          }
        } else {
          // TODO
        }
      } else {
        return;
      }
    }
  }, {
    key: 'prev',
    value: function prev() {
      var _this3 = this;

      if (!this.isRunning) {
        var maxIndex = this.maxIndex;
        var duration = this.options.duration;
        var slideBy = this.options.slideBy;
        var childs = this.baseSlider.children;
        var itemActiveClass = this._class.itemActive;
        var currentIndex = this.currentIndex;
        var firstActive = currentIndex - slideBy;

        if (this.isCarousel) {
          if (this.options.loop) {
            if (currentIndex === 1) {
              var offset = this.currentOffset + this.baseDistance;
              var go = this.go(offset, 'backward', duration);

              go.then(function () {
                _this3.go(_this3.lastOffset, 'backward', '0s');
              }).then(function () {
                _event2.default.removeClass(childs[currentIndex], itemActiveClass);
                _event2.default.addClass(childs[firstActive], itemActiveClass);
                _this3.currentIndex = maxIndex;
              }).catch(function () {
                return;
              });
            } else {
              var _offset3 = this.currentOffset + this.baseDistance;
              var _go2 = this.go(_offset3, 'backward', duration);

              _go2.then(function () {
                _event2.default.removeClass(childs[currentIndex], itemActiveClass);
                _event2.default.addClass(childs[firstActive], itemActiveClass);
                _this3.currentIndex = firstActive;
              }).catch(function () {
                return;
              });
            }
          } else {
            if (currentIndex === 0) {
              return;
            }
            this.go(this.baseDistance, 'backward', duration).then(function () {
              _event2.default.addClass(childs[currentIndex - 1], itemActiveClass);
              _event2.default.removeClass(childs[currentIndex], itemActiveClass);
              _this3.currentIndex = _this3.currentIndex - 1;
            }).catch(function () {
              return;
            });
          }
        } else {
          // TODO
        }
      } else {
        return;
      }
    }
  }, {
    key: 'gotoPage',
    value: function gotoPage(index, direction) {}
    // TODO


    // Add Slide and remove slide

  }]);

  return SSlider;
}();

exports.default = SSlider;
module.exports = exports['default'];

/***/ }),
/* 1 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
var eventHelpers = {
  addEvent: function addEvent(evnt, elem, func) {
    if (elem.addEventListener) {
      elem.addEventListener(evnt, func, false); // W3C DOM
    } else if (elem.attachEvent) {
      // IE DOM
      elem.attachEvent('on' + evnt, func);
    } else {
      // No much to do
      elem[evnt] = func;
    }
  },
  next: function next(el) {
    do {
      el = el.nextSibling;
    } while (el && el.nodeType !== 1);
    return el;
  },
  prev: function prev(el) {
    do {
      el = el.previousSibling;
    } while (el && el.nodeType !== 1);
    return el;
  },
  addClass: function addClass(el, _class) {
    el.classList.add(_class);
    /* let _mClass = el.className;
      // if (_mClass.indexOf(_class) === -1) {
    _mClass += ' ' + _class;
    _mClass = _mClass.replace(/^\s+|\s+$/g, '');
    el.className = _mClass;
    // }*/
  },
  removeClass: function removeClass(el, _class) {
    el.classList.remove(_class);
    /* let _mClass = el.className;
    let arr = _mClass.split(' ');
      _mClass = '';
    for (let i = 0, len = arr.length; i < len; i++) {
      if (arr[i] !== _class) {
        _mClass += arr[i] + ' ';
      }
    }
    _mClass = _mClass.replace(/^\s+|\s+$/g, '');
    el.className = _mClass;*/
  },
  wrap: function wrap(el, wrapper) {
    var parent = el.parentNode;
    var sibling = el.nextSibling;

    wrapper.appendChild(el);
    if (sibling) {
      parent.insertBefore(wrapper, sibling);
    } else {
      parent.appendChild(wrapper);
    }
  }
};

exports.default = eventHelpers;
module.exports = exports['default'];

/***/ }),
/* 2 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
var browserHelpers = {
  isSupport: function isSupport(p) {
    var v = ['Moz', 'webkit', 'Webkit', 'Khtml', 'O', 'ms'];
    var b = document.body || document.documentElement;
    var s = b.style;

    if (typeof s[p] === 'string') {
      return true;
    }

    p = p.charAt(0).toUpperCase() + p.substr(1);

    for (var i = 0; i < v.length; i++) {
      if (typeof s[v[i] + p] === 'string') {
        return true;
      }
    }

    return false;
  }
};

exports.default = browserHelpers;
module.exports = exports['default'];

/***/ })
/******/ ]);
});
//# sourceMappingURL=SSlider.js.map