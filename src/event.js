const eventHelpers = {
  addEvent: function (evnt, elem, func) {
    if (elem.addEventListener) {
      elem.addEventListener(evnt, func, false);  // W3C DOM
    } else if (elem.attachEvent) { // IE DOM
      elem.attachEvent('on' + evnt, func);
    } else { // No much to do
      elem[evnt] = func;
    }
  },
  next: function (el) {
    do {
      el = el.nextSibling;
    } while (el && el.nodeType !== 1);
    return el;
  },
  prev: function (el) {
    do {
      el = el.previousSibling;
    } while (el && el.nodeType !== 1);
    return el;
  },
  addClass: function (el, _class) {
    el.classList.add(_class);
    /* let _mClass = el.className;

    // if (_mClass.indexOf(_class) === -1) {
    _mClass += ' ' + _class;
    _mClass = _mClass.replace(/^\s+|\s+$/g, '');
    el.className = _mClass;
    // }*/
  },
  removeClass: function (el, _class) {
    el.classList.remove(_class);
    /* let _mClass = el.className;
    let arr = _mClass.split(' ');

    _mClass = '';
    for (let i = 0, len = arr.length; i < len; i++) {
      if (arr[i] !== _class) {
        _mClass += arr[i] + ' ';
      }
    }
    _mClass = _mClass.replace(/^\s+|\s+$/g, '');
    el.className = _mClass;*/
  },
  wrap: function (el, wrapper) {
    let parent = el.parentNode;
    let sibling = el.nextSibling;

    wrapper.appendChild(el);
    if (sibling) {
      parent.insertBefore(wrapper, sibling);
    } else {
      parent.appendChild(wrapper);
    }
  }
};

export default eventHelpers;
