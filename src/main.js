/**
 * SSlider created by ShinaBR2
 */

import eventHelpers from './event.js';
import browserHelpers from './browser.js';

export default class SSlider {
  constructor(options) {
    this.options = Object.assign({}, {
      width: null,
      height: null,
      nav: true,
      loop: true,
      dots: false,
      keyboard: true,
      touch: false,
      itemsPerPage: 1,
      startItem: 0, // zero-based
      slideBy: 1, // slideBy <= itemsPerPage
      margin: 0,
      duration: '0.5s'
    }, options);

    this.initSlide();
  }

  /**
   * Remove active class from children.
   *
   * @param      {number}  startIndex  The start index
   * @param      {number}  stopIndex   The stop index
   */
  _removeItemClass(startIndex, stopIndex) {
    let children = this.el.baseSlider.children;
    let itemActive = this._class.itemActive;

    for (let i = startIndex; i < stopIndex; i++) {
      eventHelpers.removeClass(children[i], itemActive);
    }
  }

  /**
   * Add active class to children.
   *
   * @param      {number}  startIndex  The start index
   * @param      {number}  stopIndex   The stop index
   */
  _addItemClass(startIndex, stopIndex) {
    let children = this.el.baseSlider.children;
    let itemActive = this._class.itemActive;

    for (let i = startIndex; i < stopIndex; i++) {
      eventHelpers.addClass(children[i], itemActive);
    }
  }

  /**
   * Active dot
   *
   * @param      {number}  oldIndex  The old index
   * @param      {number}  newIndex  The new index
   */
  _activeDot(oldIndex, newIndex) {
    if (this.options.dots) {
      let oldItem = oldIndex - this.firstIndex;
      let newItem = newIndex - this.firstIndex;

      eventHelpers.removeClass(this.el.dots.children[oldItem], this._class.dotActive);
      eventHelpers.addClass(this.el.dots.children[newItem], this._class.dotActive);
    }
  }

  /**
   * Active prev/next button base on index
   *
   * @param      {number}  newIndex  The new index
   */
  _activeNav(newIndex) {
    if (!this.options.loop && this.options.nav) {
      let lastIndex = this.lastIndex;
      let firstIndex = this.firstIndex;

      let nextButton = this.el.nextButton;
      let prevButton = this.el.prevButton;

      let nextDisabled = this._class.nextDisabled;
      let prevDisabled = this._class.prevDisabled;
      let itemsPerPage = this.options.itemsPerPage;

      if (newIndex <= firstIndex) {
        eventHelpers.addClass(prevButton, prevDisabled);
        eventHelpers.removeClass(nextButton, nextDisabled);
      } else if (newIndex + itemsPerPage - 1 >= lastIndex) {
        eventHelpers.addClass(nextButton, nextDisabled);
        eventHelpers.removeClass(prevButton, prevDisabled);
      } else {
        eventHelpers.removeClass(prevButton, prevDisabled);
        eventHelpers.removeClass(nextButton, nextDisabled);
      }
    }
  }

  /**
   * On keydown event handle
   *
   * @param      {Event}  e       { event }
   */
  _onKeydown(e) {
    const RIGHT_ARROW = 39;
    const LEFT_ARROR = 37;

    if (e.keyCode === RIGHT_ARROW) {
      this.next();
    } else if (e.keyCode === LEFT_ARROR) {
      this.prev();
    }
  }

  /**
   * Calculates the item width.
   *
   * @param      {number}  containerWidth  The container width
   * @param      {number}  maxVisibleItems The maximum visible items
   * @return     {number}  The item width.
   */
  _calculateItemWidth(containerWidth, maxVisibleItems) {
    let margin = this.options.margin;

    return (containerWidth - ((maxVisibleItems - 1) * margin)) / maxVisibleItems;
  }

  _afterTransform(newIndex) {
    let loop = this.options.loop;
    let itemsPerPage = this.options.itemsPerPage;

    let lastIndex = this.lastIndex;
    let currentIndex = this.currentIndex;

    let maxNewIndex = !loop && newIndex + itemsPerPage > lastIndex ? lastIndex + 1 : newIndex + itemsPerPage;
    let maxOldIndex = !loop && currentIndex + itemsPerPage > lastIndex ? lastIndex + 1 : currentIndex + itemsPerPage;

    this._activeNav(newIndex);

    this._activeDot(currentIndex, newIndex);

    this._removeItemClass(currentIndex, maxOldIndex);
    this._addItemClass(newIndex, maxNewIndex);

    this.currentIndex = newIndex;

    /**
     * Callback function
     */
    if (typeof (this.options.onTransformed) !== 'undefined') {
      this.options.onTransformed();
    }
  }

  /**
   * Init slider with given options
   */
  initSlide() {
    const dealRatio = 1.6;
    const _class = {
      mainContainer: 'sslider',
      slideContainer: 'sslider__container',
      baseSlider: 'sslider__base',
      baseSliderTransform: 'sslider__base--transform',
      item: 'sslider__item',
      itemSingle: 'sslider__item--single',
      itemActive: 'sslider__item--active',
      navBar: 'sslider__navbar',
      next: 'sslider__next',
      prev: 'sslider__prev',
      nextDisabled: 'sslider__next--disabled',
      prevDisabled: 'sslider__prev--disabled',
      dots: 'sslider__dots',
      dot: 'sslider__dot',
      dotActive: 'sslider__dot--active'
    };

    let baseSlider = document.getElementById(this.options.elmentId);
    let children = baseSlider.children;
    let totalItem = baseSlider.children.length;
    let loop = this.options.loop;
    let slideBy = this.options.slideBy;
    let startItem = this.options.startItem;
    let itemsPerPage = this.options.itemsPerPage;
    let maxVisibleItems = totalItem > itemsPerPage ? itemsPerPage : totalItem;
    let enoughItem = totalItem > itemsPerPage;

    /**
     * At now, ignored case itemsPerPage > totalItem
     */

    let containerWidth = this.options.width !== null ? this.options.width : baseSlider.parentNode.clientWidth;
    let containerHeight = this.options.height !== null ? this.options.height : (containerWidth, itemsPerPage) => {
      return itemsPerPage === 1 ? containerWidth / dealRatio : containerWidth / itemsPerPage;
    };

    let margin = this.options.margin;
    let itemWidth = this._calculateItemWidth(containerWidth, maxVisibleItems);
    let blockWidth = itemWidth + margin;
    let cloneCount = loop && enoughItem ? itemsPerPage + slideBy - 1 : 0;

    let activeItemClass = _class.itemActive;

    this._class = _class;
    this._ratio = dealRatio;

    this.el = {
      mainContainer: document.createElement('div'),
      slideContainer: document.createElement('div'),
      baseSlider: baseSlider,
      nextButton: null,
      prevButton: null,
      dots: null
    };

    this.totalItem = totalItem;

    this.firstIndex = cloneCount;
    this.lastIndex = totalItem + cloneCount - 1;// Index of last real item
    this.currentIndex = startItem + cloneCount;

    this.firstOffset = -cloneCount * blockWidth;// First real item's offsetLeft
    this.lastOffset = this.firstOffset - blockWidth * (totalItem - 1);// Last real item's offsetLeft
    this.currentOffset = -this.currentIndex * blockWidth;

    this.baseDistance = slideBy * blockWidth;// Distance for every transform slide
    this.loopDistance = blockWidth * totalItem;// Distance for translate from clone to it's real position
    this.blockWidth = blockWidth;

    this.isRunning = false;
    this.isTransform = browserHelpers.isSupport('transition');// If browser support, use translate
    this.isCarousel = itemsPerPage === 1 && slideBy === 1;

    /**
     * Callback function
     */
    if (typeof (this.options.onInit) !== 'undefined') {
      this.options.onInit();
    }

    /**
     * Add CSS classes and style for container
     */
    this.el.mainContainer.className = _class.mainContainer;
    this.el.slideContainer.className = _class.slideContainer;
    this.el.mainContainer.style.width = containerWidth + 'px';
    this.el.mainContainer.style.height = containerHeight + 'px';

    /**
     * Wrap all things to container, when finished, our slider will be something like below
     *
     * <div class="sslider" style="...">
     *     <div class="sslider__container">
     *         <div id="..." class="sslider__base ..." style="...">
     *             <!-- slide items -->
     *             <div class="sslider__item ..."></div>
     *             <!-- slide items -->
     *         </div>
     *     </div>
     * </div>
     */
    eventHelpers.wrap(this.el.baseSlider, this.el.slideContainer);
    eventHelpers.wrap(this.el.slideContainer, this.el.mainContainer);
    eventHelpers.addClass(this.el.baseSlider, _class.baseSlider);

    /**
     * If loop, add clone nodes to slider
     */
    if (loop) {
      let fragmentBefore = document.createDocumentFragment();
      let fragmentAfter = document.createDocumentFragment();
      let cloneFirst;
      let cloneLast;

      for (let i = 0; i < cloneCount; i++) {
        cloneFirst = children[i].cloneNode(true);
        cloneLast = children[totalItem - i - 1].cloneNode(true);
        cloneFirst.removeAttribute('id');
        cloneLast.removeAttribute('id');
        fragmentAfter.appendChild(cloneFirst);
        fragmentBefore.insertBefore(cloneLast, fragmentBefore.firstChild);
      }

      this.el.baseSlider.insertBefore(fragmentBefore, this.el.baseSlider.firstChild);
      this.el.baseSlider.appendChild(fragmentAfter);
    }

    /**
     * Calculate baseSlider's and items width
     */
    for (let i = 0, len = this.el.baseSlider.children.length; i < len; i++) {
      let imgSrc = children[i].querySelector('img').src;
      let childClass = _class.item;

      if (i > 0) {
        children[i].style.marginLeft = margin + 'px';
      }
      children[i].style.width = itemWidth + 'px';
      children[i].style.backgroundImage = `url(${imgSrc})`;
      children[i].style.backgroundRepeat = 'no-repeat';
      children[i].style.backgroundSize = 'cover';
      eventHelpers.addClass(children[i], childClass);

      if (i >= this.currentIndex && i < this.currentIndex + itemsPerPage) {
        eventHelpers.addClass(children[i], activeItemClass);
      }
    }
    this.el.baseSlider.style.width = this.el.baseSlider.children.length * blockWidth - margin + 'px';

    /**
     * Add CSS classes for baseSlider
     */
    if (this.isTransform) {
      this.el.baseSlider.style.WebkitTransform = 'translateX(' + this.currentOffset + 'px)';
      this.el.baseSlider.style.transform = 'translateX(' + this.currentOffset + 'px)';
      this.el.baseSlider.style.WebkitTransitionDuration = this.options.duration;
      this.el.baseSlider.style.transitionDuration = this.options.duration;
      eventHelpers.addClass(this.el.baseSlider, this._class.baseSliderTransform);
    } else {
      this.el.baseSlider.style.left = this.currentOffset + 'px';
    }

    /**
     * Next/prev button
     */
    if (this.options.nav) {
      let navBarHtml = `<button class='${this._class.prev}'>&#10092;</button>
          <button class='${this._class.next}'>&#x276D;</button>`;
      let navBar = document.createElement('div');

      navBar.innerHTML = navBarHtml;
      eventHelpers.addClass(navBar, this._class.navBar);
      this.el.mainContainer.appendChild(navBar);

      let prevButton = navBar.querySelector('.' + this._class.prev);
      let nextButton = navBar.querySelector('.' + this._class.next);

      if (!this.options.loop) {
        if (this.currentIndex + maxVisibleItems - 1 === this.lastIndex) {
          eventHelpers.addClass(nextButton, this._class.nextDisabled);
        }
        if (this.currentIndex === this.firstIndex) {
          eventHelpers.addClass(prevButton, this._class.prevDisabled);
        }
      }

      this.el.nextButton = nextButton;
      this.el.prevButton = prevButton;
      eventHelpers.addEvent('click', this.el.prevButton, this.prev.bind(this));
      eventHelpers.addEvent('click', this.el.nextButton, this.next.bind(this));
    }

    /**
     * Dots
     */
    if (this.options.dots) {
      let dotsHtml = '';
      let dots = document.createElement('div');

      for (let i = 0; i < this.totalItem; i++) {
        if (i === startItem) {
          dotsHtml += `<button class='${this._class.dot} ${this._class.dotActive}'></button>`;
        } else {
          dotsHtml += `<button class='${this._class.dot}'></button>`;
        }
      }

      dots.innerHTML = dotsHtml;
      eventHelpers.addClass(dots, this._class.dots);
      this.el.mainContainer.appendChild(dots);
      this.el.dots = dots;

      for (let j = 0; j < this.totalItem; j++) {
        eventHelpers.addEvent('click', this.el.dots.children[j], this.gotoItem.bind(this, j));
      }
    }

    /**
     * Keyboard
     */
    if (this.options.keyboard) {
      eventHelpers.addEvent('keydown', this.el.mainContainer, this._onKeydown.bind(this));
    }

    /**
     * Touch
     */
    if (this.options.touch) {
      // TODO
    }

    /**
     * Callback function
     */
    if (typeof (this.options.onInitialized) !== 'undefined') {
      this.options.onInitialized();
    }
  }

  /**
   * Gets the current item index.
   *
   * @return     {number}  The current item index.
   */
  getCurrentItem() {
    return this.currentIndex - this.firstIndex;
  }

  /**
   * Gets the total item.
   *
   * @return     {number}  The total item.
   */
  getTotalItem() {
    return this.totalItem;
  }

  /**
   * Bring slider to specified offset
   *
   * @param      {string}   offset     The offset
   * @param      {string}   direction  The direction
   * @param      {string}   duration   The duration
   * @return     {Promise}  { return nothing }
   *
   * If browser has supported, use transform
   * It not, use left instead
   */
  go(offset, direction, duration) {
    /**
     * Callback function
     */
    if (typeof (this.options.onTransform) !== 'undefined') {
      this.options.onTransform();
    }

    return new Promise((resolve, reject) => {
      if (this.isRunning) {
        reject();
      } else {
        this.isRunning = true;
        this.currentOffset = offset;

        if (this.isTransform) {
          this.el.baseSlider.style.WebkitTransitionDuration = duration;
          this.el.baseSlider.style.transitionDuration = duration;
          this.el.baseSlider.style.WebkitTransform = 'translateX(' + this.currentOffset + 'px)';
          this.el.baseSlider.style.transform = 'translateX(' + this.currentOffset + 'px)';
        } else {
          this.el.baseSlider.style.left = this.currentOffset + 'px';
        }

        setTimeout(() => {
          this.isRunning = false;
          resolve();
        }, parseFloat(duration, 10) * 1000);
      }
    });
  }

  /**
   * Bring slider to first position
   * where the first visible item is the first real item
   *
   * Just simply return in case:
   * - Slider is running
   */
  goFirst() {
    if (this.isRunning || this.currentIndex === this.firstIndex) {
      return;
    }

    this.go(this.firstOffset, 'backward', this.options.duration).then(() => {
      let newIndex = this.firstIndex;

      this._afterTransform(newIndex);
    });
  }

  /**
   * Bring slider to last position
   * where the last visible item is the last real item
   *
   * Just simply return in case:
   * - Slider is running
   */
  goLast() {
    if (this.isRunning || this.currentIndex + this.options.itemsPerPage - 1 === this.lastIndex) {
      return;
    }

    let itemsPerPage = this.options.itemsPerPage;
    let distance = (itemsPerPage - 1) * (this.baseDistance / this.options.slideBy);
    let offset = this.lastOffset + distance;

    this.go(offset, 'forward', this.options.duration).then(() => {
      let lastIndex = this.lastIndex;
      let newIndex = lastIndex - itemsPerPage + 1;

      this._afterTransform(newIndex);
    });
  }

  /**
   * Bring slider to right by `slideBy` items
   *
   * Just simply return in these cases:
   * - Slider is running
   * - No loop and current last visible item is the last real item
   */
  next() {
    let loop = this.options.loop;
    let slideBy = this.options.slideBy;
    let itemsPerPage = this.options.itemsPerPage;

    let lastIndex = this.lastIndex;
    let currentIndex = this.currentIndex;
    let newIndex = currentIndex + slideBy;
    let maxNewIndex = !loop && newIndex + itemsPerPage >= lastIndex ? lastIndex + 1 : newIndex + itemsPerPage;

    if ((this.isRunning) || (!loop && newIndex + itemsPerPage - 1 > lastIndex)) {
      return;
    }

    if ((!loop && maxNewIndex >= lastIndex) || (loop && newIndex + itemsPerPage - 1 === lastIndex)) {
      this.goLast();
    } else {
      let totalItem = this.totalItem;
      let duration = this.options.duration;

      let baseDistance = this.baseDistance;
      let loopDistance = this.loopDistance;
      let currentOffset = this.currentOffset;
      let offset = currentOffset - baseDistance;

      this.go(offset, 'forward', duration).then(() => {
        if (loop && currentIndex + slideBy > lastIndex) {
          offset = this.currentOffset + loopDistance;
          this.go(offset, 'forward', '0s').then(() => {
            newIndex = currentIndex + slideBy - totalItem;
            this._afterTransform(newIndex);
          });
        } else {
          this._afterTransform(newIndex);
        }
      });
    }
  }

  /**
   * Bring slider to left by `slideBy` items
   *
   * Just simply return in these cases:
   * - Slider is running
   * - No loop and current first visible item is the first real item
   */
  prev() {
    let loop = this.options.loop;
    let slideBy = this.options.slideBy;

    let firstIndex = this.firstIndex;
    let currentIndex = this.currentIndex;
    let newIndex = currentIndex - slideBy;

    if ((this.isRunning) || (!loop && currentIndex === firstIndex)) {
      return;
    }

    if ((!loop && newIndex <= firstIndex) || (loop && newIndex === firstIndex)) {
      this.goFirst();
    } else {
      let totalItem = this.totalItem;
      let duration = this.options.duration;

      let baseDistance = this.baseDistance;
      let loopDistance = this.loopDistance;
      let currentOffset = this.currentOffset;
      let offset = currentOffset + baseDistance;

      this.go(offset, 'backward', duration).then(() => {
        if (loop && newIndex < firstIndex) {
          offset = this.currentOffset - loopDistance;
          this.go(offset, 'backward', '0s').then(() => {
            newIndex = currentIndex - slideBy + totalItem;
            this._afterTransform(newIndex);
          });
        } else {
          this._afterTransform(newIndex);
        }
      });
    }
  }

  /**
   * Bring slider to specified item index
   * This new index item will take the first visible position
   *
   * @param      {number}  index  The index
   *
   * Just simply return in these cases:
   * - Slider is running
   * - An invalid index was given
   * - New item index equal current item index
   */
  gotoItem(index) {
    let firstIndex = this.firstIndex;
    let lastIndex = this.lastIndex;
    let currentIndex = this.currentIndex;
    let newIndex = index + firstIndex;

    if (this.isRunning || newIndex < firstIndex || newIndex > lastIndex || newIndex === currentIndex) {
      return;
    }

    let itemsPerPage = this.options.itemsPerPage;
    let offset = this.firstOffset - (newIndex - firstIndex) * this.blockWidth;
    let direction = currentIndex > newIndex ? 'backward' : 'forward';

    if (newIndex === firstIndex) {
      this.goFirst();
    } else if (newIndex + itemsPerPage - 1 === lastIndex) {
      this.goLast();
    } else {
      this.go(offset, direction, this.options.duration).then(() => {
        this._afterTransform(newIndex);
      });
    }
  }

  // Add Slide and remove slide
}
