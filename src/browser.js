const browserHelpers = {
  isSupport: function (p) {
    const v = ['Moz', 'webkit', 'Webkit', 'Khtml', 'O', 'ms'];
    let b = document.body || document.documentElement;
    let s = b.style;

    if (typeof s[p] === 'string') {
      return true;
    }

    p = p.charAt(0).toUpperCase() + p.substr(1);

    for (let i = 0; i < v.length; i++) {
      if (typeof s[v[i] + p] === 'string') {
        return true;
      }
    }

    return false;
  }
};

export default browserHelpers;
